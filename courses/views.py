from contextlib import nullcontext
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from courses.models import Course
from courses.serializers import AddInstructorSerializer, AddStudentsSerializer, CourseSerializer
from rest_framework import status
from rest_framework.authentication import TokenAuthentication

from users.models import User
from .permissions import IsAdmin
from django.core.exceptions import ObjectDoesNotExist


class CourseView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

    def post(self, request):
        
        serializer = CourseSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        course = Course.objects.filter(name__icontains=serializer.validated_data['name']).exists()

        if course:
            return Response({'message': 'Course already exists'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        new_course = Course.objects.create(**serializer.validated_data)

        serializer = CourseSerializer(new_course)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


    def get(self, request):

        courses = Course.objects.all()

        serializer = CourseSerializer(courses, many=True)

        return Response(serializer.data)


class CourseByIdView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

    def get(self, request, course_id=''):
        try:
            course = Course.objects.get(uuid=course_id)
        except ObjectDoesNotExist:
            return Response({'message': 'Course does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = CourseSerializer(course)

        return Response(serializer.data)


    def patch(self, request, course_id=''):
        try:
            course = Course.objects.filter(name__icontains=request.data['name']).exists()

            if course:
                return Response({'message': 'This course name already exists'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            course_to_update = Course.objects.get(uuid=course_id)
        except ObjectDoesNotExist:
            return Response({'message': 'Course does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = CourseSerializer(course_to_update, data=request.data, partial=True)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()

        return Response(serializer.data)

    
    def delete(self, request, course_id=''):
        try:
            course = Course.objects.get(uuid=course_id)
        except ObjectDoesNotExist:
            return Response({'message': 'Course does not exist'}, status=status.HTTP_404_NOT_FOUND)

        course.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)

class CourseInstructorView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

    def put(self, request, course_id=''):
        try:
            course = Course.objects.get(uuid=course_id)
        except ObjectDoesNotExist:
            return Response({'message': 'Course does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = AddInstructorSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        try: 
            user = User.objects.get(uuid=serializer.validated_data['instructor_id'])
        except ObjectDoesNotExist:
            return Response({'message': 'Invalid instructor_id'}, status=status.HTTP_404_NOT_FOUND)

        if not user.is_admin:
            return Response({'message': 'Instructor id does not belong to an admin'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        first_course_exists = Course.objects.filter(instructor_id=user.uuid).exists()

        if first_course_exists:
            first_course = Course.objects.get(instructor=user)
            first_course.instructor = None
            first_course.save()

        course.instructor = user
        course.save()

        serializer = CourseSerializer(course)

        return Response(serializer.data)


class CourseStudentsView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

    def put(self, request, course_id=''):
        try:
            course = Course.objects.get(uuid=course_id)
        except ObjectDoesNotExist:
            return Response({'message': 'Course does not exist'}, status=status.HTTP_404_NOT_FOUND)

        serializer = AddStudentsSerializer(data=request.data)

        students_list = []
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        for student_id in serializer.validated_data['students_id']:
            try:
                user = User.objects.get(uuid=student_id)
            except ObjectDoesNotExist:
                return Response({'message': 'Invalid students_id list'}, status=status.HTTP_404_NOT_FOUND)
            
            if user.is_admin:
                return Response({'message': 'Some student id belongs to an Instructor'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

            students_list.append(student_id)

        course.students.set(students_list)

        serializer = CourseSerializer(course)

        return Response(serializer.data)
        
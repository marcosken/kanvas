from rest_framework import serializers
from users.serializers import UserSerializer


class CourseSerializer(serializers.Serializer):
    
    uuid = serializers.UUIDField(read_only=True)

    name = serializers.CharField()

    demo_time = serializers.TimeField()

    created_at = serializers.DateTimeField(read_only=True)

    link_repo = serializers.CharField()

    instructor = UserSerializer(read_only=True)

    students = UserSerializer(many=True, read_only=True)

    
    def update(self, instance, validated_data):

        instance.name = validated_data.get('name', instance.name)

        instance.demo_time = validated_data.get('demo_time', instance.demo_time)

        instance.link_repo = validated_data.get('link_repo', instance.link_repo)

        instance.save()

        return instance


class AddInstructorSerializer(serializers.Serializer):

    instructor_id = serializers.UUIDField(format='hex_verbose')


class AddStudentsSerializer(serializers.Serializer):

    students_id = serializers.ListField(child=serializers.UUIDField(format='hex_verbose'))
from django.urls import path
from .views import CourseInstructorView, CourseStudentsView, CourseView, CourseByIdView


urlpatterns = [
    path('courses/', CourseView.as_view()),
    path('courses/<str:course_id>/', CourseByIdView.as_view()),
    path('courses/<str:course_id>/registrations/instructor/', CourseInstructorView.as_view()),
    path('courses/<str:course_id>/registrations/students/', CourseStudentsView.as_view())
]
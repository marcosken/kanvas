from django.db import models
import uuid

class Course(models.Model):

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    name = models.CharField(max_length=255)

    demo_time = models.TimeField()

    created_at = models.DateTimeField(auto_now_add=True)

    link_repo = models.CharField(max_length=255)

    instructor = models.OneToOneField('users.User', on_delete=models.CASCADE, null=True)

    students = models.ManyToManyField('users.User', related_name='courses')

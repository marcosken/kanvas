# Kanvas 👩‍🏫

## 🗂️ índice

- **[Sobre o Projeto](#-sobre-o-projeto)**
- **[Instalação](#%EF%B8%8F-instalação)**
- **[Utilização](#-utilização)**
- **[Tecnologias](#-tecnologias)**
- **[Autor](#-autor)**

## 💻 Sobre o Projeto

Este projeto trata-se de uma API em Django semelhante ao Canvas, porém de maneira mais simplificada. Seu objetivo é ser uma ferramenta para um serviço de gestão de aprendizagem online.

Com essa aplicação é possível inscrever estudantes e instrutores, registrar seus endereços, criar cursos e organizá-los da melhor maneira possível, associando-os a um instrutor e cadastrando os alunos matriculados em cada curso.

## ⚙️ Instalação

> Este projeto requer que o [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) e o [Python](https://www.python.org/downloads/) estejam instalados em sua máquina.

- Faça o fork desse repositório;

- Abra o terminal e clone o repositório:

```bash
$ git clone git@gitlab.com:<your_user>/kanvas.git
```

- Entre no diretório do projeto

```bash
$ cd kanvas
```

- Crie um ambiente virtual:

```bash
$ python3 -m venv venv
```

- Ative o ambiente virtual:

```bash
$ source venv/bin/activate
```

- Faça a instalação das dependências:

```bash
$ pip install -r requirements.txt
```

- Execute as _migrations_ para criar o banco de dados e tabelas:

```bash
$ python3 manage.py migrate
```

- Execute a aplicação:

```bash
$ python3 manage.py runserver
```

Pronto! A aplicação, agora, pode ser acessada através da rota [http://localhost:8000/](http://localhost:8000/).

## 🚀 Utilização

> Para utilizar a aplicação, é necessário o uso de uma API Client, como [Postman](https://www.postman.com/) ou [Insomnia](https://insomnia.rest/download).

### Rotas

#### Criação de usuário: `POST - /api/accounts/`

_Requisição:_

```json
{
  "first_name": "John",
  "last_name": "Doe",
  "email": "john_doe@bol.com.br",
  "password": "1234",
  "is_admin": false
}
```

_Resposta:_ **STATUS 201 - CREATED**

```json
{
  "uuid": "37c65691-d4fc-47e0-aef4-afe7b09c261f",
  "first_name": "John",
  "last_name": "Doe",
  "email": "john_doe@bol.com.br",
  "is_admin": false
}
```

#### Login de usuário: `POST - /api/login/`

_Requisição:_

```json
{
  "email": "john_doe@bol.com.br",
  "password": "1234"
}
```

_Resposta:_ **STATUS 200 - OK**

```json
{
  "token": "76a6683c3a38c40caee04af738d279753e2c4d8d"
}
```

#### Listagem de usuários: `GET - /api/accounts/`

_Requisição:_ **Sem body**<br/>
**Header - Authorization: Token <token_do_instrutor>**

_Resposta:_ **STATUS 200 - OK**

```json
[
  {
    "uuid": "61039ae4-7016-48d3-b90e-95a78b39e427",
    "first_name": "Maria",
    "last_name": "Joaquina",
    "email": "maria@bol.com.br",
    "is_admin": true
  },
  {
    "uuid": "af8af8b1-7d19-4032-b93d-04c84c66f8e1",
    "first_name": "João",
    "last_name": "Souza",
    "email": "joao@gmail.com.br",
    "is_admin": true
  },
  {
    "uuid": "5fef85a3-61e3-4e03-b6bd-ee544cd9bcdd",
    "first_name": "Joaquim",
    "last_name": "Ferreira",
    "email": "joaquim@hotmail.com.br",
    "is_admin": false
  },
  {
    "uuid": "5f2747f3-8899-49d9-b7f9-4ade850db837",
    "first_name": "Marcelo",
    "last_name": "Soares",
    "email": "marcelo@bol.com.br",
    "is_admin": false
  }
]
```

#### Cadastramento de endereço: `PUT - /api/address/`

_Requisição:_
**Header - Authorization: Token <token_do_usuário>**

```json
{
  "zip_code": "123456789",
  "street": "Rua das Flores",
  "house_number": "123",
  "city": "Curitiba",
  "state": "Paraná",
  "country": "Brasil"
}
```

_Resposta:_ **STATUS 200 - OK**

```json
{
  "uuid": "7bf9d19a-2c22-4daa-b7dc-30be78bf1047",
  "street": "Rua das Flores",
  "city": "Curitiba",
  "state": "Paraná",
  "zip_code": "123456789",
  "country": "Brasil",
  "users": [
    {
      "uuid": "38f8b8b7-8edc-4685-9369-643bb85169d2",
      "first_name": "John",
      "last_name": "Doe",
      "email": "john_doe@bol.com.br",
      "is_admin": false
    }
  ]
}
```

#### Criação de curso: `POST - /api/courses/`

_Requisição:_
**Header - Authorization: Token <token_do_instrutor>**

```json
{
  "name": "Django",
  "demo_time": "9:00",
  "link_repo": "https://gitlab.com/turma_django/"
}
```

_Resposta:_ **STATUS 200 - CREATED**

```json
{
  "uuid": "7c32c787-83c3-4994-8f45-b6ef113cde5e",
  "name": "Django",
  "demo_time": "09:00:00",
  "created_at": "2022-02-15T19:12:44.914032Z",
  "link_repo": "https://gitlab.com/turma_django/",
  "instructor": null,
  "students": []
}
```

#### Listagem de cursos: `GET - /api/courses/`

_Requisição:_ **Sem body**

_Resposta:_ **STATUS 200 - OK**

```json
[
  {
    "uuid": "87556b7e-ac9a-4a8d-9b9f-c2ea7e956a94",
    "name": "Django",
    "demo_time": "09:00:00",
    "created_at": "2022-02-15T19:08:15.927682Z",
    "link_repo": "https://gitlab.com/turma_django/",
    "instructor": null,
    "students": [
      {
        "uuid": "5fef85a3-61e3-4e03-b6bd-ee544cd9bcdd",
        "first_name": "Joaquim",
        "last_name": "Ferreira",
        "email": "joaquim@hotmail.com.br",
        "is_admin": false
      }
    ]
  },
  {
    "uuid": "696e31d3-2dd4-42b5-9d2d-4f92035a13fb",
    "name": "Node",
    "demo_time": "09:00:00",
    "created_at": "2022-02-15T19:09:29.898611Z",
    "link_repo": "https://gitlab.com/turma_node/",
    "instructor": null,
    "students": [
      {
        "uuid": "5f2747f3-8899-49d9-b7f9-4ade850db837",
        "first_name": "Marcelo",
        "last_name": "Soares",
        "email": "marcelo@bol.com.br",
        "is_admin": false
      }
    ]
  },
  {
    "uuid": "787673a7-fa4e-4bb8-9ec1-eabbba02fca5",
    "name": "React",
    "demo_time": "09:00:00",
    "created_at": "2022-02-15T19:10:43.883684Z",
    "link_repo": "https://gitlab.com/turma_django/",
    "instructor": {
      "uuid": "61039ae4-7016-48d3-b90e-95a78b39e427",
      "first_name": "Maria",
      "last_name": "Joaquina",
      "email": "maria@bol.com.br",
      "is_admin": true
    },
    "students": [
      {
        "uuid": "5f2747f3-8899-49d9-b7f9-4ade850db837",
        "first_name": "Marcelo",
        "last_name": "Soares",
        "email": "marcelo@bol.com.br",
        "is_admin": false
      },
      {
        "uuid": "f09c6c88-6cd3-4ed9-b818-432e9f8df11c",
        "first_name": "Pedro",
        "last_name": "Martins",
        "email": "pedro@bol.com.br",
        "is_admin": false
      }
    ]
  }
]
```

#### Filtragem de curso: `GET - /api/courses/<course_id>/`

_Requisição:_ **Sem body**

_Resposta:_ **STATUS 200 - OK**

```json
{
  "uuid": "87556b7e-ac9a-4a8d-9b9f-c2ea7e956a94",
  "name": "Django",
  "demo_time": "09:00:00",
  "created_at": "2022-02-15T19:08:15.927682Z",
  "link_repo": "https://gitlab.com/turma_django/",
  "instructor": null,
  "students": [
    {
      "uuid": "5fef85a3-61e3-4e03-b6bd-ee544cd9bcdd",
      "first_name": "Joaquim",
      "last_name": "Ferreira",
      "email": "joaquim@hotmail.com.br",
      "is_admin": false
    }
  ]
}
```

#### Atualização de curso: `PATCH - /api/courses/<course_id>/`

_Requisição:_
**Header - Authorization: Token <token_do_instrutor>**

```json
{
  "name": "Node", // Campo opcional
  "demo_time": "8:00", // Campo opcional
  "link_repo": "https://gitlab.com/turma_node/" // Campo opcional
}
```

_Resposta:_ **STATUS 200 - OK**

```json
{
  "uuid": "7c32c787-83c3-4994-8f45-b6ef113cde5e",
  "name": "Node",
  "demo_time": "08:00:00",
  "created_at": "2022-02-15T19:12:44.914032Z",
  "link_repo": "https://gitlab.com/turma_node/",
  "instructor": null,
  "students": []
}
```

#### Cadastro de instrutor no curso: `PUT - /api/courses/<course_id>/registrations/instructor/`

_Requisição:_
**Header - Authorization: Token <token_do_instrutor>**

```json
{
  "instructor_id": "61039ae4-7016-48d3-b90e-95a78b39e427"
}
```

_Resposta:_ **STATUS 200 - OK**

```json
{
  "uuid": "87556b7e-ac9a-4a8d-9b9f-c2ea7e956a94",
  "name": "Django",
  "demo_time": "09:00:00",
  "created_at": "2022-02-15T19:08:15.927682Z",
  "link_repo": "https://gitlab.com/turma_django/",
  "instructor": {
    "uuid": "61039ae4-7016-48d3-b90e-95a78b39e427",
    "first_name": "Maria",
    "last_name": "Joaquina",
    "email": "maria@bol.com.br",
    "is_admin": true
  },
  "students": []
}
```

#### Cadastro de estudantes no curso: `PUT /api/courses/<course_id>/registrations/students/`

_Requisição:_
**Header - Authorization: Token <token_do_instrutor>**

```json
{
  "students_id": [
    "5fef85a3-61e3-4e03-b6bd-ee544cd9bcdd",
    "5f2747f3-8899-49d9-b7f9-4ade850db837",
    "f09c6c88-6cd3-4ed9-b818-432e9f8df11c"
  ]
}
```

_Resposta:_ **STATUS 200 - OK**

```json
{
  "uuid": "87556b7e-ac9a-4a8d-9b9f-c2ea7e956a94",
  "name": "Django",
  "demo_time": "09:00:00",
  "created_at": "2022-02-15T19:08:15.927682Z",
  "link_repo": "https://gitlab.com/turma_django/",
  "instructor": {
    "uuid": "61039ae4-7016-48d3-b90e-95a78b39e427",
    "first_name": "Maria",
    "last_name": "Joaquina",
    "email": "maria@bol.com.br",
    "is_admin": true
  },
  "students": [
    {
      "uuid": "5fef85a3-61e3-4e03-b6bd-ee544cd9bcdd",
      "first_name": "Joaquim",
      "last_name": "Ferreira",
      "email": "joaquim@hotmail.com.br",
      "is_admin": false
    },
    {
      "uuid": "5f2747f3-8899-49d9-b7f9-4ade850db837",
      "first_name": "Marcelo",
      "last_name": "Soares",
      "email": "marcelo@bol.com.br",
      "is_admin": false
    },
    {
      "uuid": "f09c6c88-6cd3-4ed9-b818-432e9f8df11c",
      "first_name": "Pedro",
      "last_name": "Martins",
      "email": "pedro@bol.com.br",
      "is_admin": false
    }
  ]
}
```

#### Remoção de curso: `DELETE - /api/courses/<course_id>/`

_Requisição:_
**Header - Authorization: Token <token_do_instrutor>**

_Resposta:_ **STATUS 203 - NO CONTENT**

## 🛠 Tecnologias

Para o desenvolvimento desse projeto, foi necessária a utilização das seguintes ferramentas:

- **[Django](https://www.djangoproject.com/)**
- **[Django Rest Framework](https://www.django-rest-framework.org/)**
- **[Python](https://www.python.org/)**

## 👨‍💻 Autor

<img style="border-radius: 15%;" src="https://gitlab.com/uploads/-/system/user/avatar/8603970/avatar.png?width=400" width="70px;" alt=""/>

Marcos Kenji Kuribayashi

[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/marcos-kuribayashi/) [![Gmail Badge](https://img.shields.io/badge/-marcosken13@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white)](mailto:marcosken13@gmail.com)


---

Desenvolvido por Marcos Kenji Kuribayashi 😉

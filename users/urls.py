from django.urls import path
from .views import UserView, login


urlpatterns = [
    path('accounts/', UserView.as_view()),
    path('login/', login)
]
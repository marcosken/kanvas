from rest_framework import serializers

class UserSerializer(serializers.Serializer):
    
    uuid = serializers.UUIDField(read_only=True)
    
    email = serializers.EmailField()

    first_name = serializers.CharField()

    last_name = serializers.CharField()

    is_admin = serializers.BooleanField()

    password = serializers.CharField(write_only=True)


class LoginSerializer(serializers.Serializer):

        email = serializers.EmailField()

        password = serializers.CharField() 
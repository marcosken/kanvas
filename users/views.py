from django.shortcuts import render
from rest_framework.views import APIView
from .serializers import UserSerializer, LoginSerializer
from rest_framework.response import Response
from rest_framework import status
from .models import User
from rest_framework.decorators import api_view
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from .permissions import IsAdmin

class UserView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdmin]

    def post(self, request):

        serializer = UserSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.filter(email__icontains=serializer.validated_data['email']).exists()

        if user:
            return Response({'message': 'User already exists'}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        
        new_user = User.objects.create_user(**serializer.validated_data)

        serializer = UserSerializer(new_user)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


    def get(self, request):
        users = User.objects.all()

        serializer = UserSerializer(users, many=True)

        return Response(serializer.data)


@api_view(['POST'])
def login(request):
    
    serializer = LoginSerializer(data=request.data)

    if not serializer.is_valid():
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    user = authenticate(email=serializer.validated_data['email'], password=serializer.validated_data['password'])

    if user:
        token = Token.objects.get_or_create(user=user)[0]

        return Response({'token': token.key})
    else:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
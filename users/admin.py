from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin as UserBaseAdmin

@admin.register(User)
class UserAdmin(UserBaseAdmin):
    ...

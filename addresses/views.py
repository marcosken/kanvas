from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from users.models import User
from .models import Address
from .serializers import AddressSerializer
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

class AddressView(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def put(self, request):
        
        serializer = AddressSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.get(uuid=request.user.uuid)
        
        address = Address.objects.filter(zip_code=serializer.validated_data['zip_code'], 
                                    house_number=serializer.validated_data['house_number']).exists()

        if address:

            existing_address = Address.objects.get(zip_code=serializer.validated_data['zip_code'], 
                                                    house_number=serializer.validated_data['house_number'])

            user.address = existing_address
            user.save()

            serializer = AddressSerializer(existing_address)

            return Response(serializer.data)
        
        new_Address = Address.objects.create(**serializer.validated_data)

        user.address = new_Address
        user.save()

        serializer = AddressSerializer(new_Address)

        return Response(serializer.data) 
        
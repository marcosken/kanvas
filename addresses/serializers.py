from rest_framework import serializers
from users.serializers import UserSerializer


class AddressSerializer(serializers.Serializer):

    uuid = serializers.UUIDField(read_only=True)

    street = serializers.CharField(max_length=255)

    house_number = serializers.IntegerField()

    city = serializers.CharField(max_length=255)

    state = serializers.CharField(max_length=255)

    zip_code = serializers.CharField(max_length=255)
    
    country = serializers.CharField(max_length=255)

    users = UserSerializer(many=True, read_only=True)